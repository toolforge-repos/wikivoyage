<?php
// trans-it.php
// version 2017-08-09
// italian
$trans = array(
  'immagine=' => 'image=',
  'nome=' => 'name=',
  'tipo=' => 'type=',
  'descrizione' => 'content=',
  '{Città' => '{city',
  '{Destinazione' => '{vicinity',
  '{{Destinationlist' => '{{Destinationlist}}',
  '{{Regionlist' => '{{Regionlist}}',
  '{{Citylist' => '{{Citylist}}',
  '{{RigaPatrimonio' => '{{RigaPatrimonio}}'
);
?>
