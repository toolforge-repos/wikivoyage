<?php
// version 2015-09-16
// spanisch
$trans = array(
  '{ver' => '{see',
  '{hacer' => '{do',
  '{comprar' => '{buy',
  '{comer' => '{eat',
  '{beber' => '{drink',
  '{dormir' => '{sleep',
  '{listado' => '{listing',
  '=otro' => '=other',
  'tipo=' => 'type=',
  'nombre=' => 'name=',
  'imagen=' => 'image='
);
?>
