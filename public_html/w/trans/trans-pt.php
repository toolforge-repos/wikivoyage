<?php
// version 2016-09-7
// portugisisch
$trans = array(
  '{veja' => '{see',
  '{faça' => '{do',
  '{compre' => '{buy',
  '{coma' => '{eat',
  '{beba' => '{drink',
  '{durma' => '{sleep',
  '{outroitem' => '{listing',
  'tipo=' => 'type=',
  'nome=' => 'name=',
  'imagem=' => 'image=',
  '=outro' => '=other'
);
?>
