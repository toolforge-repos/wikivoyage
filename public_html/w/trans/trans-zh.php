<?php
// version 2016-09-17
// chinesisch
$search = array(
  '{观光' => '{see',
  '{觀光' => '{see',
  '{活动' => '{do',
  '{活動' => '{do',
  '{购物' => '{buy',
  '{購物' => '{buy',
  '{饮食' => '{eat',
  '{飲食' => '{eat',
  '{夜生活' => '{drink',
  '{住宿' => '{sleep',
  '{其他' => '{other',
  '类型=' => 'type=',
  '類型=' => 'type='
);
?>
