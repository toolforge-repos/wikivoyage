// Localization English -> English <syntaxhighlight lang="javascript">

// change only the texts on the right to your language

var mylocale = {

  "Boundaries": "Boundaries",
  "Cycling": "Cycling",
  "Destinations": "Destinations",
  "GPX_tracks_Map_mask": "GPX tracks / Map mask",
  "Hiking": "Hiking",
  "Hill_shading": "Hill shading",
  "Mapnik": "Mapnik",
  "Mapnik_bw": "Mapnik b&amp;w",
  "Mapquest_aerial": "Mapquest aerial",
  "Mapquest_labels": "Mapquest labels",
  "Mapquest_open": "Mapquest open",
  "Monuments": "Monuments",
  "Points_of_interest" : "Points of interest",
  "Relief_map": "Relief map",
  "Traffic_line_network": "Traffic line network",
  "WV_articles": "WV articles",
  "Zoom level": "Zoom level",
  
  "But the maximum number for downloading is 25.": "But the maximum number for downloading is 25.",
  "Content with {external} is hosted externally, so enabling it shares your data with other sites.": "Content with {external} is hosted externally, so enabling it shares your data with other sites.",
  "Data, imagery and map information provided by": "Data, imagery and map information provided by",
  "Download GPX file": "Download GPX file",
  "Download this {nn} GPX files?": "Download this {nn} GPX files?",
  "ERROR: Coordinates must be numeric!": "ERROR: Coordinates must be numeric!",
  "Locate!": "Locate!",
  "Map center ⇔ all markers": "Map center ⇔ all markers",
  "Map data": "Map data",
  "No files available for download!": "No files available for download!",
  "POIs ⇔ destinations": "POIs ⇔ destinations",
  "Please select a smaller range.": "Please select a smaller range.",
  "Show me all markers": "Show me all markers",
  "Show me the whole earth": "Show me the whole earth",
  "Show me where I am": "Show me where I am",
  "Sorry, that location could not be found.": "Sorry, that location could not be found.",
  "Version": "Version",
  "You clicked the map at": "You clicked the map at",
  "You have chosen {nn} articles.": "You have chosen {nn} articles.",
  "Zoom in": "Zoom in",
  "Zoom out": "Zoom out",
  "contributors": "contributors",
  "contributors, Tiles": " contributors, Tiles",
  "geocoded articles": "geocoded articles"
};
