// Localization English -> German <syntaxhighlight lang="javascript">

// change only the texts on the right to your language

var mylocale = {

  "Boundaries": "Grenzen",
  "Cycling": "Radwege",
  "Destinations": "Reiseziele",
  "GPX_tracks_Map_mask": "GPX Spuren / Kartenmaske",
  "Hiking": "Wanderwege",
  "Hill_shading": "Schummerung",
  "Mapnik": "Mapnik",
  "Mapnik_bw": "Mapnik s/w",
  "Mapquest_aerial": "Mapquest aerial",
  "Mapquest_labels": "Mapquest Beschriftungen",
  "Mapquest_open": "Mapquest open",
  "Monuments": "Denkmäler",
  "Points_of_interest" : "Sehenswürdigkeiten",
  "Relief_map": "Reliefkarte",
  "Traffic_line_network": "Verkehrsliniennetz",
  "WV_articles": "WV Artikel",
  "Zoom level": "Zoomstufe",
  
  "But the maximum number for downloading is 25.": "aber die maximale Anzahl zum Herunterladen ist 25.",
  "Content with {external} is hosted externally, so enabling it shares your data with other sites.": "Inhalte mit {external} werden extern gehostet. Ihre IP-Daten werden dort sichtbar, falls diese Inhalte ausgewählt werden.",
  "Data, imagery and map information provided by": "Daten, Bilder und Karteninformationen bereitgestellt durch",
  "Download GPX file": "GPX Datei herunterladen",
  "Download this {nn} GPX files?": "Diese {nn} GPX-Dateien herunterladen?",
  "ERROR: Coordinates must be numeric!": "FEHLER: Koordinaten müssen numerisch sein!",
  "Locate!": "Finde!",
  "Map center ⇔ all markers": "Kartenzentrum ⇔ alle Marker",
  "Map data": "Kartendaten",
  "No files available for download!": "Keine Artikel in diesem Bereich!",
  "POIs ⇔ destinations": "POI ⇔ Reiseziele",
  "Please select a smaller range.": "Bitte wählen Sie einen kleineren Bereich.",
  "Show me all markers": "zeig mir alle Marker",
  "Show me the whole earth": "zeig mir die gesamte Erde",
  "Show me where I am": "zeige meinen Standort",
  "Sorry, that location could not be found.": "Leider konnte dieser Ort nicht gefunden werden.",
  "Version": "Version",
  "You clicked the map at": "Sie klickten auf die Karte bei",
  "You have chosen {nn} articles.": "Sie haben {nn} Artikel ausgewählt,",
  "Zoom in": "vergrößern",
  "Zoom out": "verkleinern",
  "contributors": "Mitwirkende",
  "contributors, Tiles": "Mitwirkende, Kartenkacheln",
  "geocoded articles": "geokodierte Artikel"
};
