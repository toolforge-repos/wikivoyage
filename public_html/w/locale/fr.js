// Localization English -> French <syntaxhighlight lang="javascript">

// change only the texts on the right to your language

var mylocale = {

  "Boundaries": "Frontières",
  "Cycling": "Cycliste",
  "Destinations": "Destinations",
  "GPX_tracks_Map_mask": "GPX tracks / Masque de carte",
  "Hiking": "Randonnée",
  "Hill_shading": "Ombré le relief",
  "Mapnik": "Mapnik",
  "Mapnik_bw": "Mapnik n&amp;b",
  "Mapquest_aerial": "Mapquest vue aérienne",
  "Mapquest_labels": "Mapquest annotations",
  "Mapquest_open": "Mapquest ouvert",
  "Monuments": "Monuments",
  "Points_of_interest" : "Points d\'intérêt",
  "Relief_map": "Carte en relief",
  "Traffic_line_network": "Lignes de transport public",
  "WV_articles": "Articles WV",
  "Zoom level": "Niveau de zoom",
  
  "But the maximum number for downloading is 25.": "Mais le nombre maximum pour le téléchargement est 25.",
  "Content with {external} is hosted externally, so enabling it shares your data with other sites.": "Contenu avec {external} est hébergé à l'extérieur, permettant ainsi un partage de vos données avec d'autres sites.",
  "contributors": "contributeurs",
  "contributors, Tiles": " contributeurs, Tiles",
  "Data, imagery and map information provided by": "donnée, imagerie et information cartographie fourni par",
  "Download GPX file": "Télécharger le fichier GPX",
  "Download this {nn} GPX files?": "Télécharger ce fichier GPX {nn}?",
  "ERROR: Coordinates must be numeric!": "ERREUR: Les coordonnées doivent être numérique!",
  "geocoded articles": "articles géocodés",
  "Locate!": "Localise!",
  "Map center ⇔ all markers": "Centre de la carte ⇔ tout les symboles",
  "Map data": "Map data",
  "No files available for download!": "Pas de fichier disponible au téléchargement!",
  "Please select a smaller range.": "S'il vous plait, sélectionnez une gamme plus restreinte.",
  "POIs ⇔ destinations": "Point d\'intérêt ⇔ destinations",
  "Show me all markers": "Afficher tout les symboles",
  "Show me the whole earth": "Afficher la terre entière",
  "Show me where I am": "Afficher ma position",
  "Sorry, that location could not be found.": "Désolé, ce lieu ne peut pas être trouvé.",
  "Version": "Version",
  "You clicked the map at": "Vous avez cliqué sur la carte à",
  "You have chosen {nn} articles.": "Vous avez choisi {nn} articles.",
  "Zoom in": "Zoom +",
  "Zoom out": "Zoom -"
};
