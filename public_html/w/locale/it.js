// Localization English --> Italian <syntaxhighlight lang="javascript">

// change only the texts on the right to your language

var mylocale = {

  "Boundaries": "Confini",
  "Cycling": "Piste ciclabili",
  "Destinations": "Destinazioni",
  "GPX_tracks_Map_mask": "Tracciati GPX e maschere",
  "Hiking": "Percorsi escursionistici",
  "Hill_shading": "Rilievi collinari",
  "Mapnik": "Mapnik",
  "Mapnik_bw": "Mapnik b&amp;w",
  "Mapquest_aerial": "Mapquest aerial",
  "Mapquest_labels": "Mapquest labels",
  "Mapquest_open": "Mapquest open",
  "Monuments": "Monumenti",
  "Points_of_interest" : "Punti di interesse",
  "Relief_map": "Mappa dei rilievi",
  "Traffic_line_network": "Rete dei trasporti",
  "WV_articles": "Articoli WV",
  "Zoom level": "Livello dello zoom",
  
  "But the maximum number for downloading is 25.": "Ma il massimo numero di download è 25.",
  "Content with {external} is hosted externally, so enabling it shares your data with other sites.": "I contenuti con {external} sono ospitati esternamente, quindi abilitandoli si condividerà i propri dati con terze parti.",
  "Data, imagery and map information provided by": "Dati, immagini e informazioni topografiche fornite da",
  "Download GPX file": "Scarica file GPX",
  "Download this {nn} GPX files?": "Scarica questi {nn} file GPX?",
  "ERROR: Coordinates must be numeric!": "ERRORE: Le coordinate devono essere numeriche!",
  "Locate!": "Trova!",
  "Map center ⇔ all markers": "Centro mappa ⇔ tutti i segnaposto",
  "Map data": "Dati della mappa",
  "No files available for download!": "Nessun file disponibile per il download!",
  "POIs ⇔ destinations": "POI ⇔ destinazioni",
  "Please select a smaller range.": "Si prega di scegliere un intorno più piccolo.",
  "Show me all markers": "Mostra tutti i segnaposto",
  "Show me the whole earth": "Mostra l'intero globo",
  "Show me where I am": "Mostra dove sono",
  "Sorry, that location could not be found.": "Sono spiacente, che il luogo cercato non è stato trovato.",
  "Version": "Versione",
  "You clicked the map at": "Hai fatto click nella mappa su",
  "You have chosen {nn} articles.": "Hai scelto {nn} articoli.,",
  "Zoom in": "Ingrandisci",
  "Zoom out": "Rimpicciolisci",
  "contributors": "Contributori",
  "contributors, Tiles": "Contributori, mappe",
  "geocoded articles": "Articoli geocodificati"
};
